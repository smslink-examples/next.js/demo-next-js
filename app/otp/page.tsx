'use client';
import { FieldValues, useForm } from "react-hook-form";
import { useState } from "react";
import toast from "react-hot-toast";
import { FormEvent } from 'react';

const Button = (props) => {
  return <>
    <button className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
      {props.label}
    </button>
  </>
}

export default function GeneratePage() {

  // Define a state that the condition will be based upon
  const [step, setStep] = useState('generate');


  const {
    register,
    resetField,
    getValues,
    setError,
    formState: { errors }
  } = useForm<FieldValues>({
    defaultValues: {
      phone: "",
      code: "",
    }
  });


  const [states, setStates] = useState({
    phone_submitted: false,
    code_sent: false,
    code_submitted: false,
    code_validated: false,
  });

  function start() {

    setStates({ ...states, phone_submitted: false });
    setStates({ ...states, code_sent: false });
    setStates({ ...states, code_submitted: false });
    setStates({ ...states, code_validated: false });

    resetField('phone');
    resetField('code');

    setStep('generate');
  }

  async function onSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();

    let url;

    if (step == 'generate') {

      url = '/api/otp-generate';

      if (!states.phone_submitted) {
        setStates({ ...states, phone_submitted: true });
      } else {
        return false;
      }

    } else if (step == 'validate') {
      url = '/api/otp-validate';

      if (!states.code_submitted) {
        setStates({ ...states, code_submitted: true });
      } else {
        return false;
      }

    } else {
      start();
      return false;
    }


    const response = await fetch(url, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(getValues()),
    })

    // Handle response if necessary
    const data = await response.json();

    if (!response.ok) {

      // erreurs de validation
      if (response.status == 422) {
        console.error(data.data);

        //  toast.error(data.data.message); // Displays an error  message

        if (typeof data.data.errors != "undefined") {
          if (typeof data.data.errors.phone_number != "undefined") {

            setStates({ ...states, phone_submitted: false });
            setError("phone", { type: "focus", message: data.data.errors.phone_number[0] }, { shouldFocus: true });
          }

          if (typeof data.data.errors.code != "undefined") {
            setStates({ ...states, code_submitted: false });
            setError("code", { type: "focus", message: data.data.errors.code }, { shouldFocus: true });
          }
        }

      } else if (response.status == 420) {
        toast.error('Trop de tentatives, veuillez recommencer');
        setStep('restart');
      }

    } else {

      console.log(data.data);

      if (step == 'generate') {
        const fullNumber = getValues("phone");
        const last4Digits = fullNumber.slice(-2);
        const maskedNumber = last4Digits.padStart(fullNumber.length, '*');

        toast.success('Saisissez le code de vérification envoyé au +687 ' + maskedNumber, { duration: 5000 }); // Displays a success message
        setStates({ ...states, code_sent: true });
        setStep('validate');
      } else {
        setStates({ ...states, code_validated: true });
        toast.success('Code validé'); // Displays a success message
        setStep('restart');
      }
    }
  }


  return (
    <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={onSubmit}>

      {step == 'generate' &&
        <div>
          <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="phone">
              Téléphone
            </label>
            <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="phone"
              {...register("phone")}
              type="phone"
              required
              maxLength={6}
              placeholder="700000"
            />

            {errors.phone && <p className="text-red-500 text-xs italic">{errors.phone.message}</p>}

          </div>

          <div className="mb-4">
            <Button label="Envoyer un code" />
          </div>
        </div>

      }

      {step == 'validate' &&
        <div>
          <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="code">
              Code de vérification
            </label>
            <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="code"
              {...register("code")}
              type="text"
              required
            />

            {errors.code && <p className="text-red-500 text-xs italic">{errors.code.message}</p>}
          </div>

          <div className="mb-4">
            <Button label="Valider le code" />
          </div>

        </div>
      }

      {step == 'restart' &&

        <div className="mb-4">
          <Button label="Recommencer" />
        </div>
      }

    </form>
  );



}