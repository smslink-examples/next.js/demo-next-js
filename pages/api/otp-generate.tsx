import type {
  NextApiRequest,
  NextApiResponse
} from 'next';

type ResponseData = {
  data: object
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData>
) {

  const endpoint = `${process.env.SMSLINK_API_BASE_URL}/service/otp/generate`;
  const body = req.body;

  debugger;
  console.log('phone', body.phone);

  const requestBody = {
    text: "Bonjour,\nvoici votre code d'authentification: {code}\nil expirera à {hh:mm}",
    code_length: 4,
    allow_digits: true,
    allow_upper_case: false,
    allow_lower_case: false,
    allow_special_chars: false,
    validity_period: 3600,
    max_wrong_attempts: process.env.SMSLINK_MAX_WRONG_ATTEMPTS,
    phone_number: body.phone
  };

  // Appel API SMS Link
  const response = await fetch(endpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${process.env.SMSLINK_API_TOKEN}`,
      "Accept-Language": "fr-FR",
    },
    body: JSON.stringify(requestBody)
  });

  const data = await response.json();

  debugger;
  console.log(data, response.status);

  res.status(response.status).json({
    data: data
  })

}