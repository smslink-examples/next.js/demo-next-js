import type {
  NextApiRequest,
  NextApiResponse
} from 'next';

type ResponseData = {
  data: object
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData>
) {

  const endpoint = `${process.env.SMSLINK_API_BASE_URL}/service/otp/validate`;
  const body = req.body;

  const requestBody = {
    code: body.code,
    phone_number: body.phone
  };

  // Appel API SMS Link
  const response = await fetch(endpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${process.env.SMSLINK_API_TOKEN}`,
      "Accept-Language": "fr-FR",
    },
    body: JSON.stringify(requestBody)
  });

  const data = await response.json();

  debugger;
  console.log(data, response.status);

  res.status(response.status).json({
    data: data
  })

}